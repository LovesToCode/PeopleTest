/*
 * Copyright 2021 Steven Anthony (Tony) Williams.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.saw.people.model;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Data model for user info.
 * The structure matches the server model exactly,
 * @author Steven Anthony (Tony) Williams
 */
public class User
{
   private long id;
   private String userName;
   private String password;

   public User()
   {
      this.userName = "Unknown";
      this.password = "password";
   }
   public User(String userName, String password)
   {
      this.userName = userName;
      this.password = password;
   }
   public User(JSONObject json)
   {
      _setJson(json);
   }

   public long getId() { return id; }
   public void setId(long id) { this.id = id; }

   public String getUserName() { return userName; }
   public void setUserName(String userName) { this.userName = userName; }

   public String getPassword() { return password; }
   public void setPassword(String password) { this.password = password; }

   public void json(JSONObject json)
   {
      _setJson(json);
   }

   public JSONObject json()
   {
      return new JSONObject().put("id", this.id)
                             .put("userName", this.userName)
                             .put("password", this.password);
   }

   @Override
   public String toString()
   {
      StringBuilder strBuilder = new StringBuilder();

      strBuilder.append("   Id: ")
                .append(id)
                .append("\n")
                .append("   User Name: ")
                .append(userName)
                .append("\n")
                .append("   Password: ")
                .append(password)
                .append("\n");

      return strBuilder.toString();
   }

   /////////////////
   // Static Methods

   public static String userListToString(List<User> users)
   {
      StringBuilder strBldr = new StringBuilder();

      strBldr.append("Users:\n");
      for (User user : users)
      {
         strBldr.append(user.toString())
                .append("--------------\n");
      }

      return strBldr.toString();
   }

   public static List<User> jsonArrayToList(JSONArray jArray)
   {
      ArrayList<User> users = new ArrayList<>();

      for (int u = 0; u < jArray.length(); u++)
         users.add(new User(jArray.getJSONObject(u)));

      return users;
   }

   //////////////////
   // Private Methods

   private void _setJson(JSONObject json)
   {
      this.id = _jsonGetLong(json, "id");
      this.userName = _jsonGetString(json, "userName");
      this.password = _jsonGetString(json, "password");
   }

   private Long _jsonGetLong(JSONObject json, String fieldName)
   {
      try { return json.getLong(fieldName); }
      catch (JSONException ex) { return -1L; }
   }

   private String _jsonGetString(JSONObject json, String fieldName)
   {
      try { return json.getString(fieldName); }
      catch (JSONException ex) { return null; }
   }
}
