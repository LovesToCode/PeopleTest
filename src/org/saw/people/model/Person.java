/*
 * Copyright 2021 Steven Anthony (Tony) Williams.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.saw.people.model;

import java.util.List;
import java.util.stream.Collectors;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Data model for person info.
 * The structure matches the server model exactly,
 * minus the <code>SpringBoot</code> annotations.
 * @author Steven Anthony (Tony) Williams
 */
public class Person
{
   private long id;
   private String firstName;
   private String lastName;
   private String eMail;
   private String phone;
   private long userId;

   public Person() { }
   public Person(String firstName,
                 String lastName,
                 String eMail,
                 String phone)
   {
      this.firstName = firstName;
      this.lastName = lastName;
      this.eMail = eMail;
      this.phone = phone;
      this.userId = 0;
   }
   public Person(JSONObject json)
   {
      _setJson(json);
   }

   public long getId() { return id; }
   public void setId(long id) { this.id = id; }

   public String getFirstName() { return firstName; }
   public void setFirstName(String firstName) { this.firstName = firstName; }

   public String getLastName() { return lastName; }
   public void setLastName(String lastName) { this.lastName = lastName; }

   public String getEMail() { return eMail; }
   public void setEMail(String eMail) { this.eMail = eMail; }

   public String getPhone() { return phone; }
   public void setPhone(String phone) { this.phone = phone; }

   public long getUserId() { return userId; }
   public void setUserId(long userId) { this.userId = userId; }

   public void json(JSONObject json)
   {
      _setJson(json);
   }

   public JSONObject json()
   {
      JSONObject j = new JSONObject();

      return j.put("id", id)
              .put("firstName", firstName)
              .put("lastName", lastName)
              .put("email", eMail)
              .put("phone", phone)
              .put("userId", userId);
   }

   @Override
   public String toString()
   {
      StringBuilder strBuilder = new StringBuilder();

      strBuilder.append("   Id: ")
                .append(id)
                .append("\n")
                .append("   Name: ")
                .append(firstName)
                .append(" ")
                .append(lastName)
                .append("\n")
                .append("   EMail: ")
                .append(eMail)
                .append("\n")
                .append("   Phone: ")
                .append(phone)
                .append("\n")
                .append("   User Id: ")
                .append(userId)
                .append("\n");

      return strBuilder.toString();
   }

   /////////////////
   // Static Methods

   public static String peopleListToString(List<Person> people, List<User> users)
   {
      StringBuilder strBldr = new StringBuilder();

      strBldr.append("People:\n");
      for (Person person : people)
      {
         long pId = person.getUserId();

         User user = users.stream()
                          .filter(u -> u.getId() == pId)
                          .findFirst()
                          .orElse(new User());

         strBldr.append(person.toString())
                .append("\n")
                .append(user.toString())
                .append("--------------\n");
      }

      return strBldr.toString();
   }

   //////////////////
   // Private Methods

   private void _setJson(JSONObject json)
   {
      this.id = _jsonGetLong(json, "id");
      this.firstName = _jsonGetString(json, "firstName");
      this.lastName = _jsonGetString(json, "lastName");
      this.eMail = _jsonGetString(json, "email");
      this.phone = _jsonGetString(json, "phone");
      this.userId = _jsonGetLong(json, "userId");
   }

   private Long _jsonGetLong(JSONObject json, String fieldName)
   {
      try { return json.getLong(fieldName); }
      catch (JSONException ex) { return -1L; }
   }

   private String _jsonGetString(JSONObject json, String fieldName)
   {
      try { return json.getString(fieldName); }
      catch (JSONException ex) { return null; }
   }
}
