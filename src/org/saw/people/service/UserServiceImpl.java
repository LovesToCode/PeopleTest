/*
 * Copyright 2021 Steven Anthony (Tony) Williams.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.saw.people.service;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.saw.people.model.User;

/**
 *
 * @author Steven Anthony (Tony) Williams
 */
public class UserServiceImpl implements UserService
{
   private final HttpClient client;
   private final String uriBase;
   private Consumer<User> binder;

   public UserServiceImpl(HttpClient client, String uriBase)
   {
      this.client = client;
      this.uriBase = uriBase;
   }

   @Override
   public void setBinder(Consumer<User> binder)
   {
      this.binder = binder;
   }

   @Override
   public List<User> getAllUsers()
   {
      JSONArray json = new JSONArray(_sendGetWithPath("getAllUsers()", "user"));
      ArrayList<User> uList = new ArrayList<>();
      String jStr;
      JSONObject jObj;
      User jUser;

      for (int j = 0; j < json.length(); j++)
      {
         try
         {
            jStr = json.optString(j);
            jObj = new JSONObject(jStr);
            jUser = new User(jObj);

            uList.add(jUser);
         }
         catch (JSONException ex)
         {
            Logger.getLogger(UserServiceImpl.class.getSimpleName())
                    .log(Level.SEVERE, null, ex);
         }
      }

      return uList;
   }

   @Override
   public User createUser(User user)
   {
      try
      {
         String jsonStr = user.json().toString();

         HttpRequest request = HttpRequest.newBuilder()
                 .uri(URI.create(uriBase + "user/create"))
                 .header("Content-Type", "application/json")
                 .POST(BodyPublishers.ofString(jsonStr))
                 .build();
         HttpResponse<String> response;
         User newUser;

         response = client.send(request, BodyHandlers.ofString());
         newUser = new User(new JSONObject(response.body()));

         if (binder != null)
            binder.accept(newUser);

         return newUser;
      }
      catch (IOException | InterruptedException ex)
      {
         Logger.getLogger(UserServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
         return null;
      }
   }

   @Override
   public User readUser(long id)
   {
      String jsonStr = _sendGetWithPath("readUser()", "user/read/" + id);

      if (jsonStr != null)
      {
         User newUser = new User(new JSONObject(jsonStr));

         if (binder != null)
            binder.accept(newUser);

         return newUser;
      }
      else
         return null;
   }
/*
   @Override
   public User updateUserName(long userId, String userName)
   {
      return null;
   }

   @Override
   public User updatePassword(long userId, String password)
   {
      return null;
   }
*/
   @Override
   public User updateUser(long userId, User aUser)
   {
      try
      {
         String jsonUser = aUser.json().toString();
         HttpRequest request = HttpRequest.newBuilder()
                 .uri(URI.create(uriBase + "user/update/" + userId))
                 .header("Content-Type", "application/json")
                 .PUT(BodyPublishers.ofString(jsonUser))
                 .build();

         HttpResponse<String> response;
         User newUser;

         response = client.send(request, BodyHandlers.ofString());
         newUser = new User(new JSONObject(response.body()));

         if (binder != null)
            binder.accept(newUser);

         return newUser;
      }
      catch (IOException | InterruptedException ex)
      {
         Logger.getLogger(PeopleServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
         return null;
      }
   }

   @Override
   public String deleteUser(long userId)
   {
      String retString = _initErrorString("deleteUser(Long, Person)");

      try
      {
         HttpRequest request = HttpRequest.newBuilder()
                 .uri(URI.create(uriBase + "user/delete/" + userId))
                 .DELETE()
                 .build();
         HttpResponse<String> response;

         response = client.send(request, BodyHandlers.ofString());
         retString = response.body();
      }
      catch (IOException | InterruptedException ex)
      {
         Logger.getLogger(PeopleServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
         retString = _setErrorString(retString, ex.getMessage());
      }

      return retString;
   }

   //////////////////
   // Private Methods

   private String _initErrorString(String method)
   {
      return "ERROR; [" + UserServiceImpl.class.getName() + "." + method;
   }

   private String _setErrorString(String error, String message)
   {
      return error + " - " + message + "]";
   }

   private String _sendGetWithPath(String methodName, String path)
   {
      String retString = _initErrorString(methodName);

      try
      {
         HttpRequest request = HttpRequest.newBuilder()
                 .uri(URI.create(uriBase + path))
                 .build();
         HttpResponse<String> response;

         response = client.send(request, BodyHandlers.ofString());
         retString = response.body();
      }
      catch (IOException | InterruptedException ex)
      {
         Logger.getLogger(UserServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
         retString = _setErrorString(retString, ex.getMessage());
      }

      return retString;
   }
/*
   private String _updateFieldWithPath(String message, String path, long userId, String fieldStr)
   {
      String retString = _initErrorString(message);

      try
      {
         HttpRequest request = HttpRequest.newBuilder()
                 .uri(URI.create(uriBase + "user/update/" + userId + "/" + path))
                 .header("Content-Type", "application/json")
                 .PUT(BodyPublishers.ofString(fieldStr))
                 .build();

         HttpResponse<String> response;
         response = client.send(request, BodyHandlers.ofString());
         retString = response.body();
      }
      catch (IOException | InterruptedException ex)
      {
         Logger.getLogger(UserServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
         retString = _setErrorString(retString, ex.getMessage());
      }

      return retString;
   }
*/
}
