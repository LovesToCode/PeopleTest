/*
 * Copyright 2021 Steven Anthony (Tony) Williams.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.saw.people.service;

import java.util.List;
import java.util.function.Consumer;
import org.saw.people.model.Person;

/**
 *
 * @author Steven Anthony (Tony) Williams
 */
public interface PeopleService
{
   void setBinder(Consumer<Person> binder);
   String getGreeting();
   List<Person> getAllPeople();
   Person createPerson(Person person);
   Person readPerson(long id);
   Person updateFirstName(long personId, String firstName);
   Person updateLastName(long personId, String lastName);
   Person updateEMail(long personId, String email);
   Person updatePhone(long personId, String phone);
   Person updatePerson(long personId, Person aPerson);
   String deletePerson(long personId);
}
