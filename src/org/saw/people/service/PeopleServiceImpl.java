/*
 * Copyright 2021 Steven Anthony (Tony) Williams.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.saw.people.service;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.saw.people.model.Person;

/**
 *
 * @author Steven Anthony (Tony) Williams
 */
public class PeopleServiceImpl implements PeopleService
{
   private final HttpClient client;
   private final String uriBase;
   private Consumer<Person> binder;

   public PeopleServiceImpl(HttpClient client, String uriBase)
   {
      this.client = client;
      this.uriBase = uriBase;
   }

   @Override
   public void setBinder(Consumer<Person> binder)
   {
      this.binder = binder;
   }

   @Override
   public String getGreeting()
   {
      return _sendGetWithPath("getGreeting()", "");
   }

   @Override
//   @SuppressWarnings("unchecked")
   public List<Person> getAllPeople()
   {
      JSONArray json = new JSONArray(_sendGetWithPath("getAllPeople()", "people"));
      ArrayList<Person> pList = new ArrayList<>();
      String jStr;
      JSONObject jObj;
      Person jPer;

      for (int j = 0; j < json.length(); j++)
      {
         try
         {
            jStr = json.optString(j);
            jObj = new JSONObject(jStr);
            jPer = new Person(jObj);

            pList.add(jPer);
         }
         catch (JSONException ex)
         {
            Logger.getLogger(PeopleServiceImpl.class.getSimpleName())
                    .log(Level.SEVERE, null, ex);
         }
      }

      return pList;
   }

   @Override
   public Person createPerson(Person person)
   {
      try
      {
         String jsonStr = person.json().toString();

         HttpRequest request = HttpRequest.newBuilder()
                 .uri(URI.create(uriBase + "people/create"))
                 .header("Content-Type", "application/json")
                 .POST(BodyPublishers.ofString(jsonStr))
                 .build();
         HttpResponse<String> response;
         Person newPerson;

         response = client.send(request, BodyHandlers.ofString());
         newPerson = new Person(new JSONObject(response.body()));

         if (binder != null)
            binder.accept(newPerson);

         return newPerson;
      }
      catch (IOException | InterruptedException ex)
      {
         Logger.getLogger(PeopleServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
         return null;
      }
   }

   @Override
   public Person readPerson(long id)
   {
      String jsonStr = _sendGetWithPath("readPerson()", "people/read/" + id);

      if (jsonStr != null)
      {
         Person newPerson = new Person(new JSONObject(jsonStr));

         if (binder != null)
            binder.accept(newPerson);

         return newPerson;
      }
      else
         return null;
   }

   @Override
   public Person updateFirstName(long personId, String firstName)
   {
      String jsonStr = _updateFieldWithPath("updateFirstName(Long, String)", "firstname", personId, firstName);
      Person newPerson = new Person(new JSONObject(jsonStr));

      if (binder != null)
         binder.accept(newPerson);

      return jsonStr != null ? newPerson : null;
   }

   @Override
   public Person updateLastName(long personId, String lastName)
   {
      String jsonStr = _updateFieldWithPath("updateLastName(Long, String)", "lastname", personId, lastName);
      Person newPerson = new Person(new JSONObject(jsonStr));

      if (binder != null)
         binder.accept(newPerson);

      return jsonStr != null ? newPerson : null;
   }

   @Override
   public Person updateEMail(long personId, String email)
   {
      String jsonStr = _updateFieldWithPath("updateEMail(Long, String)", "email", personId, email);
      Person newPerson = new Person(new JSONObject(jsonStr));

      if (binder != null)
         binder.accept(newPerson);

      return jsonStr != null ? newPerson : null;
   }

   @Override
   public Person updatePhone(long personId, String phone)
   {
      String jsonStr = _updateFieldWithPath("updatePhone(Long, String)", "phone", personId, phone);
      Person newPerson = new Person(new JSONObject(jsonStr));

      if (binder != null)
         binder.accept(newPerson);

      return jsonStr != null ? newPerson : null;
   }

   @Override
   public Person updatePerson(long personId, Person aPerson)
   {
      try
      {
         String jsonPerson = aPerson.json().toString();
         HttpRequest request = HttpRequest.newBuilder()
                 .uri(URI.create(uriBase + "people/update/" + personId))
                 .header("Content-Type", "application/json")
                 .PUT(BodyPublishers.ofString(jsonPerson))
                 .build();

         HttpResponse<String> response;
         Person newPerson;

         response = client.send(request, BodyHandlers.ofString());
         newPerson = new Person(new JSONObject(response.body()));

         if (binder != null)
            binder.accept(newPerson);

         return newPerson;
      }
      catch (IOException | InterruptedException ex)
      {
         Logger.getLogger(PeopleServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
         return null;
      }
   }

   @Override
   public String deletePerson(long personId)
   {
      String retString = _initErrorString("deletePerson(Long, Person)");

      try
      {
         HttpRequest request = HttpRequest.newBuilder()
                 .uri(URI.create(uriBase + "people/delete/" + personId))
                 .DELETE()
                 .build();
         HttpResponse<String> response;

         response = client.send(request, BodyHandlers.ofString());
         retString = response.body();
      }
      catch (IOException | InterruptedException ex)
      {
         Logger.getLogger(PeopleServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
         retString = _setErrorString(retString, ex.getMessage());
      }

      return retString;
   }

   //////////////////
   // Private Methods

   private String _initErrorString(String method)
   {
      return "ERROR; [" + PeopleServiceImpl.class.getName() + "." + method;
   }

   private String _setErrorString(String error, String message)
   {
      return error + " - " + message + "]";
   }

   private String _sendGetWithPath(String methodName, String path)
   {
      String retString = _initErrorString(methodName);

      try
      {
         HttpRequest request = HttpRequest.newBuilder()
                 .uri(URI.create(uriBase + path))
                 .build();
         HttpResponse<String> response;

         response = client.send(request, BodyHandlers.ofString());
         retString = response.body();
      }
      catch (IOException | InterruptedException ex)
      {
         Logger.getLogger(PeopleServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
         retString = _setErrorString(retString, ex.getMessage());
      }

      return retString;
   }

   private String _updateFieldWithPath(String message, String path, long personId, String fieldStr)
   {
      String retString = _initErrorString(message);

      try
      {
         HttpRequest request = HttpRequest.newBuilder()
                 .uri(URI.create(uriBase + "people/update/" + personId + "/" + path))
                 .header("Content-Type", "application/json")
                 .PUT(BodyPublishers.ofString(fieldStr))
                 .build();

         HttpResponse<String> response;
         response = client.send(request, BodyHandlers.ofString());
         retString = response.body();
      }
      catch (IOException | InterruptedException ex)
      {
         Logger.getLogger(PeopleServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
         retString = _setErrorString(retString, ex.getMessage());
      }

      return retString;
   }
}
