/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.saw.people.service;

import java.net.http.HttpClient;

/**
 * Create a new service for use in the client.
 * @author Steven Anthony (Tony) Williams
 */
public final class PeopleServiceFactory
{
   private PeopleServiceFactory() {}

   public static PeopleService createPeopleService(HttpClient client, String uri)
   {
      return new PeopleServiceImpl(client, uri);
   }
}
