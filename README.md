A Java Swing program that tests my personApi project. It was created using Apache Netbeans 12.5.

Needed: https://gitlab.com/LovesToCode/personapi (the db service to test)
Needed: https://gitlab.com/LovesToCode/org.json (dependency)

Build, and run the personapi Spring Boot database service. Run this project.
